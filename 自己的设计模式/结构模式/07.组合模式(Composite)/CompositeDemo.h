//
//  CompositeDemo.h
//  组合模式(Composite)
//
//  Created by mark on 2020/2/8.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

//1.定义: 组合模式将对象组合成树形结构以表示“部分-整体”的层次结构，使得用户对单个对象和组合对象的使用具有一致性。
//2. 使用场景： 维护和展示部分-整体关系的场景，在具有整体和部分的层次结构中，希望通过一种方式忽略整体与部分的差异，可以一致地对待它们的时候。
//
//4.优点： 1.高层模块调用简单。2.节点自由增加，而不必更改原来代码。3.叶子对象可以被组合成更复杂的容器对象，而这个容器对象又可以被组合，这样不断递归下去，可以形成复杂的树形结构。
//5.缺点： 使设计变得更加抽象，对象的业务规则如果很复杂，则实现组合模式具有很大挑战性，而且不是所有的方法都与叶子对象子类都有关联。
//6.注意事项: 当使用这个属性结构的调用组件能够通过同一个类或者协议来使用书中包含的所有的对象时，才能证明正确的实现了此模式。



NS_ASSUME_NONNULL_BEGIN

@protocol FileSystem <NSObject>

- (void)displayWithHierarchy:(NSUInteger)hierarchy;//展示层级的方法

@end

@interface Folder : NSObject<FileSystem>

@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, strong,readonly) NSMutableArray *filesArray;

- (instancetype)initWithName:(NSString *)name;
- (void)addFile:(id <FileSystem>)file;

@end

@interface File : NSObject<FileSystem>

@property (nonatomic, copy, readonly) NSString *name;

- (instancetype)initWithName:(NSString *)name;

@end

@interface HeaderFile : File

@end

@interface ImplementationFile : File

@end
NS_ASSUME_NONNULL_END
