//
//  ProxyDemo.m
//  代理模式
//
//  Created by mark on 2020/2/8.
//  Copyright © 2020 mark. All rights reserved.
//

#import "ProxyDemo.h"

@implementation TrainStation

- (void)sell {
    NSLog(@"火车票已经成功卖出");
}

@end

@interface TicketOutlets()

@property (nonatomic, strong) TrainStation *trainStain;

@end

@implementation TicketOutlets

- (instancetype)init {
    if (self = [super init]) {
        _trainStain = [[TrainStation alloc] init];
    }
    return self;
}

- (void)sell {
    NSLog(@"收取代售手续费5元");
    [_trainStain sell];
}

@end
