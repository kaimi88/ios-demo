//
//  ProxyDemo.h
//  代理模式
//
//  Created by mark on 2020/2/8.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>
//1.定义: 代理模式为其他对象提供一种代理以控制对这个对象的访问。
//2. 使用场景： 想在访问一个类时做一些控制。
//4.优点： 1、职责清晰。 2、高扩展性。
//5.缺点： 增加了系统的复杂度
//6.注意事项: 1、和适配器模式的区别：适配器模式主要改变所考虑对象的接口，而代理模式不能改变所代理类的接口。 2、和装饰器模式的区别：装饰器模式为了增强功能，而代理模式是为了加以控制。

NS_ASSUME_NONNULL_BEGIN

@protocol Ticket <NSObject>

- (void)sell;

@end

@interface TrainStation : NSObject<Ticket>

@end

@interface TicketOutlets : NSObject<Ticket>//代理TrainStation对象，实现对TrainStation对象的控制

@end
NS_ASSUME_NONNULL_END
