//
//  WrapperDemo2.h
//  适配器模式案例2
//
//  Created by mark on 2020/2/6.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UserInfoProtocol <NSObject>

@required

- (NSString *)getName;

- (NSString *)getAge;

@end

@interface ModelOne : NSObject

@property (nonatomic,strong) NSString *userName;

@property (nonatomic,strong) NSString *age;

@end

@interface ModelTwo : NSObject

@property (nonatomic,strong) NSString *firstName;

@property (nonatomic,strong) NSString *lastName;

@property (nonatomic,assign) NSInteger age;

@end

@interface ModelOneAdapter : NSObject <UserInfoProtocol>

- (instancetype)initWithModel:(ModelOne *)model;

@end

@interface ModelTwoAdapter : NSObject <UserInfoProtocol>

- (instancetype)initWithModel:(ModelTwo *)model;

@end

@interface Client : NSObject
- (void)loadModel:(id<UserInfoProtocol>)model;
@end
