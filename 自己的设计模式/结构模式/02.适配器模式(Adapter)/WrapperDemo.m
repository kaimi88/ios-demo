//
// Created by mark on 2020/2/6.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//

#import "WrapperDemo.h"

/*
 适配器模式(Adapter Pattern) ：
 将一个接口转换成客户希望的另一个接口，使得原本由于接口不兼容而不能一起工作的那些类可以一起工作。
 只让客户端与适配器通信.。

 */

/*
 符合开闭原则：使用适配器而不需要改变现有类，提高类的复用性。
目标类和适配器类解耦，提高程序扩展性。
 */

/*
 Target：目标抽象类
 Adapter：适配器类
 Adaptee：适配者类
 Client：客户类
 */
@implementation AdapterUSD

- (float)getUSD{
    return 100.0f;
}

@end

@interface AdapterManager ()
@property (nonatomic,strong)AdapterUSD *adapterUSD;
@end

@implementation AdapterManager

- (instancetype)initWithAdapter:(AdapterUSD *)adapter{
    self = [super init];
    if (self) {
        _adapterUSD = adapter;
    }
    return self;
}

- (float)getCNY{
    NSLog(@"%lf",[self.adapterUSD getUSD]*6.6);
    return [self.adapterUSD getUSD]*6.6;
}

@end





