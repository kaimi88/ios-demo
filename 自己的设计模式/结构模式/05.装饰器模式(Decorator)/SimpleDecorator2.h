//
//  SimpleDecorator2.h
//  装饰器模式
//
//  Created by mark on 2020/2/7.
//  Copyright © 2020 mark. All rights reserved.
//

//1.定义: 装饰模式能动态的给一个对象添加一些额外的职责。就增加功能来说，装饰模式会比通过继承生成子类更为灵活。
//2. 使用场景： 需要动态地给一个对象增加功能，这些功能也可以动态地被撤销。
//3. 具体实现： Objective-C中的Category 就是装饰器模式的一种应用。这里举了一个鸡肉堡的例子，具体Demo点击查看
//4.优点： 装饰器模式中定义的行为，能够在不创建大量子类的情况下，组合起来实现复杂的效果，比继承更加灵活。
//5.缺点： 装饰器模式会导致设计中出现许多的小对象，会让系统变得更加复杂，比如说出错调试时寻找错误可能需要逐级排查。
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol Humburger <NSObject>
- (NSString *)getDescription;
- (NSInteger)getCost;
@end

@interface ChickenBurger : NSObject<Humburger>

@end

@interface Condiment : NSObject<Humburger>

@property (nonatomic, strong, readonly) ChickenBurger *burger;

- (instancetype)initWithChickenBurger:(id<Humburger>)burger;

@end

@interface Chilli : Condiment

@end

@interface Egg : Condiment

@end

NS_ASSUME_NONNULL_END
