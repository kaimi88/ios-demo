//
//  SimpleDecorator.m
//  装饰器模式
//
//  Created by mark on 2020/2/7.
//  Copyright © 2020 mark. All rights reserved.
//

#import "SimpleDecorator.h"

@implementation Espresso{
    NSString *_name;
}

- (instancetype)init{
    if (self = [super init]) {
        _name = @"Espresso";
    }
    return self;
}

- (NSString *)getName{
    return _name;
}

- (double)cost{
    return 1.99;
}
@end

@implementation DarkRoast{
    NSString *_name;
}

- (instancetype)init{
    if (self = [super init]) {
        _name = @"DarkRoast";
    }
    return self;
}

- (NSString *)getName{
    return _name;
}

- (double)cost{
    return 3.0;
}

@end


@implementation Milk{
    NSString *_name;
}

- (instancetype)initWithBeverage:(id<Beverage>)beverage{
    if (self = [super init]) {
        _name = @"Milk";
        self.beverage = beverage;
    }
    return self;
}
- (NSString *)getName{
    NSLog(@"%@", [NSString stringWithFormat:@"%@ + %@", [self.beverage getName], _name]);
    return [NSString stringWithFormat:@"%@ + %@", [self.beverage getName], _name ];
}

- (double)cost{
    NSLog(@"%lf",.30 + [self.beverage cost]);
    return .30 + [self.beverage cost];
}
@end

@implementation Mocha{
    NSString *_name;
}

- (instancetype)initWithBeverage:(id<Beverage>)beverage{
    if (self = [super init]) {
        self.beverage = beverage;
        _name = @"Mocha";
    }
    return self;
}

- (NSString *)getName{
    NSLog(@"%@",[NSString stringWithFormat:@"%@ + %@", [self.beverage getName], _name]);
    return [NSString stringWithFormat:@"%@ + %@", [self.beverage getName], _name];
}

- (double)cost{
    NSLog(@"%lf", .20 + [self.beverage cost]);
    return .20 + [self.beverage cost];
}

@end

