//
//  SimpleDecorator2.m
//  装饰器模式
//
//  Created by mark on 2020/2/7.
//  Copyright © 2020 mark. All rights reserved.
//

#import "SimpleDecorator2.h"

@implementation ChickenBurger

- (NSString *)getDescription {
    return @"鸡肉堡";
}
- (NSInteger)getCost {
    return 10;
}

@end

@implementation Condiment

- (instancetype)initWithChickenBurger:(id<Humburger>)burger {
    if (self = [super init]) {
        _burger = burger;
    }
    return self;
}

- (NSString *)getDescription {
    return @"";
}
- (NSInteger)getCost {
    return 0;
}

@end

@implementation Chilli

- (NSString *)getDescription {
    return [NSString stringWithFormat:@"%@+辣椒粉", [self.burger getDescription]];
}
- (NSInteger)getCost {
    return [self.burger getCost] + 1;
}

@end

@implementation Egg

- (NSString *)getDescription {
    return [NSString stringWithFormat:@"%@+煎蛋", [self.burger getDescription]];
}
- (NSInteger)getCost {
    return [self.burger getCost] + 2;
}

@end
