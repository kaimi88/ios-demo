//
//  FlyweightDemo.h
//  享元模式
//
//  Created by mark on 2020/2/9.
//  Copyright © 2020 mark. All rights reserved.
//

//1.定义: 享元模式就是运行共享技术有效地支持大量细粒度对象的复用
//2. 使用场景： 系统中存在大量的相似对象，由于这类对象的大量使用可能会造成系统内存资源浪费，而且这些对象的状态大部分可以外部化，这个时候可以考虑享元模式。在iOS中，我们用到的UITableView 重用机制就是享元模式的典型应用。
//3. 具体实现： 这里通过了一个画圆的Demo来演示一下享元模式， 具体Demo请点击这里查看
//4.优点： 通过共享极大的减少了对象实例的个数，节省了内存开销。
//5.缺点： 1.提高了系统的复杂度，需要分离出外部状态和内部状态。 2.这些类必须有一个工厂对象加以控制。
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol Shape <NSObject>

- (void)draw;

@end

@interface Circle : NSObject<Shape>

- (instancetype)initWithColorString:(NSString *)colorString;

@end

@interface FlyweightFactory : NSObject

- (Circle *)createCircleWithColorString:(NSString *)colorString;

- (void)getCount;

@end

NS_ASSUME_NONNULL_END
