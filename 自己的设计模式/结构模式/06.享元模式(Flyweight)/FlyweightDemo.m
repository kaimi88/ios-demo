//
//  FlyweightDemo.m
//  享元模式
//
//  Created by mark on 2020/2/9.
//  Copyright © 2020 mark. All rights reserved.
//

#import "FlyweightDemo.h"

@interface Circle()

@property (nonatomic, copy) NSString *colorString;

@end

@implementation Circle

- (instancetype)initWithColorString:(NSString *)colorString {
    if (self = [super init]) {
        _colorString = colorString;
    }
    return self;
}

- (void)draw {
    NSLog(@"画了一个%@的圆形", _colorString);
}

@end


@interface FlyweightFactory()

@property (nonatomic, strong) NSDictionary *circleDictionary;

@end

@implementation FlyweightFactory

- (instancetype)init {
    if (self = [super init]) {
        _circleDictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (Circle *)createCircleWithColorString:(NSString *)colorString {//节省内存开销
    Circle *circle = [_circleDictionary objectForKey:colorString];
    if (circle == nil) {
        circle = [[Circle alloc] initWithColorString:colorString];
        [_circleDictionary setValue:circle forKey:colorString];
    }
    return circle;
}

- (void)getCount {
    NSLog(@"一共创建了%ld个圆", _circleDictionary.count);
}


@end
