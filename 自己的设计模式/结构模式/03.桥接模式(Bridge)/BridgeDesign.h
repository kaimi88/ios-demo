//
//  BridgeDesign.h
//  桥接模式
//
//  Created by mark on 2020/2/8.
//  Copyright © 2020 mark. All rights reserved.
//

//套路 先定义接口、在定义数据、然后定义实体类
//桥接模式就是将抽象部分和实现部分解耦，从而使得两者可以独立的变化。

//桥接模式 优先考虑组合 而非继承

//相同的东西保留一份, 将不同的东西抽象出来


#import <Foundation/Foundation.h>



NS_ASSUME_NONNULL_BEGIN
@protocol App <NSObject>

+ (NSString *)moduleDescription;

@end

@protocol ThemeInfo <NSObject>

- (NSString *)getColor;

@end

@interface Theme : NSObject<ThemeInfo>

@end

@interface Module : NSObject<App>

+ (void)moduleDescriptionWithTheme:(Theme *)theme;

@end

@interface BlueTheme : Theme

@end

@interface RedTheme : Theme

@end


@interface MineModule : Module

@end

@interface ChatModule : Module

@end

NS_ASSUME_NONNULL_END
