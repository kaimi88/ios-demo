//
//  BridgeDesign.m
//  桥接模式
//
//  Created by mark on 2020/2/8.
//  Copyright © 2020 mark. All rights reserved.
//

#import "BridgeDesign.h"

@implementation Theme

- (NSString *)getColor {
    return @"";
}

@end

@implementation Module

+ (NSString *)moduleDescription {
    return @"";
}

+ (void)moduleDescriptionWithTheme:(Theme *)theme {
}

@end

@implementation RedTheme

- (NSString *)getColor {
    return @"red";
}

@end

@implementation BlueTheme

- (NSString *)getColor {
    return @"blue";
}

@end

@implementation MineModule

+ (NSString *)moduleDescription {
    NSLog(@"我的模块");
    return @"我的模块";
}

//抽象部分和实现部分实现了解耦
+ (void)moduleDescriptionWithTheme:(Theme *)theme {
    NSLog(@"%@-%@", [MineModule moduleDescription], [theme getColor]);
}

@end


@implementation ChatModule

+ (NSString *)moduleDescription {
    NSLog(@"聊天模块");
    return @"聊天模块";
}

+ (void)moduleDescriptionWithTheme:(Theme *)theme {
     NSLog(@"%@-%@", [ChatModule moduleDescription], [theme getColor]);
}

@end
