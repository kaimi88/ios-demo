//
//  SimpleDecorator.h
//  装饰器模式
//
//  Created by mark on 2020/2/7.
//  Copyright © 2020 mark. All rights reserved.
//

//装饰模式(Decorator Pattern) ：不改变原有对象的前提下，动态地给一个对象增加一些额外的功能，装饰模式比生成子类实现更为灵活

//场景：星巴克里有几十种饮料，如果我们给每种咖啡都创建一个类继承父类，最终会导致类数目过于庞大，而且星巴克也在不断地推出新的品种，这会给我们系统的维护带来不小的麻烦。

//成员:
//Component: 抽象构件
//ConcreteComponent: 具体构件
//Decorator: 抽象装饰类
//ConcreteDecorator: 具体装饰类

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol Beverage <NSObject>//饮料

@optional
- (NSString *)getName;//名称
- (double)cost;//价格

@end

@protocol CondimentDecorator <Beverage>//调料

@end

@interface Espresso : NSObject<Beverage>//被装饰者
@end

@interface DarkRoast : NSObject<Beverage>//被装饰者
@end


@interface Milk : NSObject <CondimentDecorator>//装饰器
@property (strong, nonatomic)id<Beverage> beverage;
- (instancetype)initWithBeverage:(id<Beverage>) beverage;
@end

@interface Mocha : NSObject<CondimentDecorator>//装饰器
@property (strong, nonatomic)id<Beverage> beverage;
- (instancetype)initWithBeverage:(id<Beverage>) beverage;
@end


NS_ASSUME_NONNULL_END
