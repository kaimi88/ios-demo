//
//  FacadeDemo.h
//  外观模式
//
//  Created by mark on 2020/2/9.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>
//1.定义: 外观模式要求一个子系统的外部与内部的通信必须通过一个统一的对象进行。外观模式提供一个高层次的接口，用来访问子系统中的一群接口。
//2. 使用场景： 当一个复杂子系统需要提供一个简单的调用接口时可以使用外观模式。
//3. 具体实现： 外观模式比较容易理解，这里举一个家电管家的例子，命令起床，命令睡觉。家电管家帮你做关灯，拉窗帘等等一系列操作。 
//4.优点： 使用此模式可以将复杂的API代码隐藏到一个简单的接口中，减少调用者直接对复杂API的依赖和耦合。修改时也只需要修改简单的接口即可。
//5.缺点： 不太遵守开闭原则，一旦发现有一些操作的时候，或者在增加新的子系统的时候，可能需要修改外观类代码，可能会造成一些风险。
NS_ASSUME_NONNULL_BEGIN
@interface HouseManager : NSObject//外观模式通过一个公共的mamanger 对象 访问内部接口
- (void)sleep;

- (void)getUp;
@end

@interface Light : NSObject

+ (void)on;
+ (void)off;

@end

@interface Curtain : NSObject

+ (void)up;
+ (void)down;

@end
NS_ASSUME_NONNULL_END
