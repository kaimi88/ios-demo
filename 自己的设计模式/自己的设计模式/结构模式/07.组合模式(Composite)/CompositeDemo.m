//
//  CompositeDemo.m
//  组合模式(Composite)
//
//  Created by mark on 2020/2/8.
//  Copyright © 2020 mark. All rights reserved.
//

#import "CompositeDemo.h"
@implementation Folder

- (instancetype)initWithName:(NSString *)name {
    if (self = [super init]) {
        _name = name;
        _filesArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)displayWithHierarchy:(NSUInteger)hierarchy {
    NSInteger temp = hierarchy;
    hierarchy++;
    NSMutableString *prefix = [[NSMutableString alloc] initWithString:@""];
    while (temp--) {
        [prefix appendString:@"-"];
    }
    NSLog(@"%@目录:%@", prefix, self.name);
    for (NSInteger i = 0; i < _filesArray.count; i++) {
        [_filesArray[i] displayWithHierarchy:hierarchy];
    }
}

- (void)addFile:(id <FileSystem>)file {
    [_filesArray addObject:file];
}

@end

@implementation File

- (instancetype)initWithName:(NSString *)name {
    if (self = [super init]) {
        _name = name;
    }
    return self;
}

- (void)displayWithHierarchy:(NSUInteger)hierarchy {
    
}

@end

@implementation HeaderFile

- (void)displayWithHierarchy:(NSUInteger)hierarchy {
    
    NSMutableString *prefix = [[NSMutableString alloc] initWithString:@""];
    while (hierarchy--) {
        [prefix appendString:@"-"];
    }
    NSLog(@"%@头文件:%@.h", prefix, self.name);
}

@end


@implementation ImplementationFile

- (void)displayWithHierarchy:(NSUInteger)hierarchy {
    NSMutableString *prefix = [[NSMutableString alloc] initWithString:@""];
    while (hierarchy--) {
        [prefix appendString:@"-"];
    }
    NSLog(@"%@实现文件:%@.m", prefix, self.name);
}

@end




