//
//  WrapperDemo2.m
//  适配器模式案例2
//
//  Created by mark on 2020/2/6.
//  Copyright © 2020 mark. All rights reserved.
//

#import "WrapperDemo2.h"

@implementation ModelOne

@end

@implementation ModelTwo

@end

@interface ModelOneAdapter ()
    
@property (nonatomic, strong) ModelOne *model; /**< 输入的模型数据 */

@end

@implementation ModelOneAdapter

- (instancetype)initWithModel:(ModelOne *)model {

    self = [super init];
    if (self) {
        self.model = model;
    }
    return self;
}

#pragma mark - UserInfoProtocol

- (NSString *)getName {
    return  self.model.userName;
}

- (NSString *)getAge {
    return self.model.age;
}

@end


@interface ModelTwoAdapter ()

@property (nonatomic, strong) ModelTwo *model; /**< 输入的模型数据 */

@end

@implementation ModelTwoAdapter

- (instancetype)initWithModel:(ModelTwo *)model {

    self = [super init];
    if (self) {
        self.model = model;
    }
    return self;
}

#pragma mark - UserInfoProtocol

- (NSString *)getName {
    return  [NSString stringWithFormat:@"%@ %@",self.model.lastName,self.model.firstName];
}

- (NSString *)getAge {
    return  [NSString stringWithFormat:@"%ld",(long)self.model.age];
}

@end

@implementation Client

- (void)loadModel:(id <UserInfoProtocol>)model {
    
    NSLog(@"UI展示用户姓名：%@，展示用户年龄：%@", [model getName], model.getAge);
}

@end

