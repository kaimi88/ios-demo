//
// Created by mark on 2020/2/6.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//

/*
 适配器模式(Adapter Pattern) ：
 将一个接口转换成客户希望的另一个接口，使得原本由于接口不兼容而不能一起工作的那些类可以一起工作。
 只让客户端与适配器通信.。

 */

/*
 符合开闭原则：使用适配器而不需要改变现有类，提高类的复用性。
目标类和适配器类解耦，提高程序扩展性。
 */

/*
 Target：目标抽象类
 Adapter：适配器类
 Adaptee：适配者类
 */
#import <Foundation/Foundation.h>


@protocol MoneyAdapterProtocol<NSObject>// Target：目标抽象类
-(float)getCNY;
@end

@interface AdapterUSD : NSObject// Adapter
- (float)getUSD;
@end


@interface AdapterManager : NSObject<MoneyAdapterProtocol>// 对象适配器
- (instancetype)initWithAdapter:(AdapterUSD *)adapter;
@end
