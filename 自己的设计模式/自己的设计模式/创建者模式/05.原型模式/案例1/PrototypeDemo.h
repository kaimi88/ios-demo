//
// Created by mark on 2020/2/6.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
  原型模式（Prototype Pattern）:
  使用原型实例指定待创建对象的类型，并且通过复制这个原型来创建新的对象
 */

/*
适用场景：
对象层级嵌套比较多，从零到一创建对象的过程比较繁琐时，可以直接通过复制的方式创建新的对象

当一个类的实例只能有几个不同状态组合中的一种时，我们可以利用已有的对象进行复制来获得
 */

/*
 成员:
抽象原型类(Prototype)：抽象原型类声明克隆自身的接口。
具体原型类（ConcretePrototype）：具体原型类实现克隆的具体操作（克隆数据，状态等）
。*/



//1. 定义：原型模式是通过克隆已有的对象来创建新的对象，已有的对象称为原型。通俗来讲，原型模式就是允许你创建现有对象的副本并根据需要进行修改，而不是从头开始创建对象并进行设置。
//2. 使用场景：通过初始化产生一个对象需要非常繁琐的准备步骤，也就是新生成一个对象的代价比较大，则可以考虑使用原型模式。

// 当有大量对象需要创建的时候 可以节省内存


@interface BaseCopyObject : NSObject <NSCopying>

// 子类不要重载
- (id)copyWithZone:(NSZone *)zone;

// 子类去实现
- (void)copyOperationWithObject:(id)object;

@end

@interface StudentModel : BaseCopyObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *age;

@end

@interface ClassModel : BaseCopyObject

@property (nonatomic, copy) NSString *className;
@property (nonatomic, copy) NSArray *students;

@end

