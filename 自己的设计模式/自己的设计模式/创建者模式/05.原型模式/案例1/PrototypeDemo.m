//
// Created by mark on 2020/2/6.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//

#import "PrototypeDemo.h"

@implementation BaseCopyObject

- (id)copyWithZone:(NSZone *)zone {
    BaseCopyObject *copyObject = [[self class] allocWithZone:zone];
    // 赋值操作
    [self copyOperationWithObject:copyObject];
    return copyObject;
}

- (void)copyOperationWithObject:(id)object {
}

@end

@implementation StudentModel

- (void)copyOperationWithObject:(StudentModel *)object {
    object.name = self.name;
    object.age = self.age;
}

@end

@implementation ClassModel
- (void)copyOperationWithObject:(ClassModel *)object {
    object.className = self.className;
    // 深拷贝
    object.students = [[NSArray alloc] initWithArray:self.students copyItems:YES];
}

@end

