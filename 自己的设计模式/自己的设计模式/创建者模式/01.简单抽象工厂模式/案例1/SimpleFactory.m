//
// Created by mark on 2020/2/3.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//

#import "SimpleFactory.h"

@class Product_AA, Product_BB;

@implementation SimpleFactory
+ (id <Productprotocol>)createProduct:(NSString *)className {
    if ([className isEqualToString:NSStringFromClass(Product_AA.class)]) {
        return [[Product_AA alloc] init];
    } else if ([className isEqualToString:NSStringFromClass(Product_BB.class)]) {
        return [[Product_BB alloc] init];
    } else {
        return nil;
    }
}
@end

@implementation Product_AA
- (void)productMethod {
    NSLog(@"生产a产品");
}

@end

@implementation Product_BB
- (void)productMethod {
    NSLog(@"生产B产品");
}

@end
