//
// Created by mark on 2020/2/3.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//

/*简单工厂模式：(Simple Factory Pattern)：
1、专门定义一个类（工厂类）来负责创建不同类的实例
2、可以根据创建方法的参数来返回不同类的实例，被创建的实例通常都具有共同的父类。
3、将一些为数不多的类似的对象的创建和他们的创建细节分离开，也不需要知道对象的具体类型。
 */

/*
 成员:
 1、工厂（Factory）：工厂负责实现创建所有产品实例的逻辑
 2、抽象产品（Product）：抽象产品是工厂所创建的所有产品对象的父类，负责声明所有产品实例所共有的公共接口。(java有abstract，ios里用协议)
 3、具体产品（Concrete Product）：具体产品是工厂所创建的所有产品对象类，它以自己的方式来实现其共同父类声明的接口。
 */




#import <Foundation/Foundation.h>

@protocol Productprotocol <NSObject>
- (void)productMethod;
@end

@interface SimpleFactory : NSObject
+ (id <Productprotocol>)createProduct:(NSString *)className;
@end

@interface Product_AA : NSObject <Productprotocol>
@end

@interface Product_BB : NSObject <Productprotocol>
@end

