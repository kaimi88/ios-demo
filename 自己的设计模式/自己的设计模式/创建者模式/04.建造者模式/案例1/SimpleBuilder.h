//
// Created by mark on 2020/2/5.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//
/*
抽象建造者（Builder）：定义构造产品的几个公共方法。
具体建造者（ConcreteBuilder）：根据不同的需求来实现抽象建造者定义的公共方法；每一个具体建造者都包含一个产品对象作为它的成员变量。
指挥者（Director）：根据传入的具体建造者来返回其所对应的产品对象。(构造一个使用Builder接口的对象)
产品角色（Product）：创建的产品。
 */

//优点:是客户不知道具体的生产步骤，将生产步骤和过程解耦，可以相同的步骤(有子类实现不同过程)生产不同的产品，扩展方便，只需要(子类)实现每个步骤，
//缺点:是多个产品相似点很多才能抽象出来步骤。



//：场景： 1.将产品的创建过程与产品本身分离开来，可以使用相同的创建过程来得到不同的产品。
//             2.每一个具体建造者都相对独立，因此可以很方便地替换具体建造者或增加新的具体建造者。

#import <Foundation/Foundation.h>

@class Phone;

@protocol Builder <NSObject>

@optional
- (void)createPhone;

- (void)buildCPU;

- (void)buildCapacity;

- (void)buildDisplay;

- (void)buildCamera;

- (Phone *)obtainPhone;
@end

@interface Phone : NSObject
@property(nonatomic, copy) NSString *cpu;
@property(nonatomic, copy) NSString *capacity;
@property(nonatomic, copy) NSString *display;
@property(nonatomic, copy) NSString *camera;
- (void)getDescription;
@end

@interface IPhoneXRBuilder : NSObject <Builder>

@end

@interface MIPhoneXRBuilder : NSObject <Builder>
@end

@interface Director : NSObject

+ (Phone *)constructPhoneWithBuilder:(id<Builder>)builder;

@end


