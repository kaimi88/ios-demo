//
//  HandWheatCakeBuilder.h
//  MLDesignPatterns-OC
//
//  Created by mjpc on 2017/8/22.
//  Copyright © 2017年 mali. All rights reserved.
//


//：场景： 1.将产品的创建过程与产品本身分离开来，可以使用相同的创建过程来得到不同的产品。
//             2.每一个具体建造者都相对独立，因此可以很方便地替换具体建造者或增加新的具体建造者。

//3.建造者模式所创建的产品一般具有较多的共同点，其组成部分相似，如果产品之间的差异性很大，则不适合使用建造者模式，

#import <Foundation/Foundation.h>
//手抓饼
@protocol HandWheatCakeBuilder <NSObject>

- (void)addFood;
- (void)addcondiment;

@end
