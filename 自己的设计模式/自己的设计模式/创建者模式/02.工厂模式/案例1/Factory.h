//
// Created by mark on 2020/2/3.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//


//1.定义: 定义一个用于创建对象的接口，让子类决定实例化哪一个类。
//2. 使用场景： 当存在多个类共同实现一个协议或者共同继承一个基类的时候，需要创建不同的对象，这个时候就可以考虑是否有必要使用工厂类进行管理。

#import <Foundation/Foundation.h>

@protocol ProductProtocol <NSObject>
- (void)productMethod;
@end

@protocol FactoryProtocol <NSObject>
+ (id <ProductProtocol>)createProduct:(NSString *)productName;
@end

@interface Factory_A : NSObject <FactoryProtocol>
@end

@interface Factory_B : NSObject <FactoryProtocol>
@end

@interface Product_A : NSObject <ProductProtocol>
@end

@interface Product_B : NSObject <ProductProtocol>

@end
