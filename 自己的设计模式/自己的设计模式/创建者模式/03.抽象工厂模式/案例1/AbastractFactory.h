//
// Created by mark on 2020/2/4.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ProductA <NSObject>
- (void)productMethod;
@end

@protocol ProductB <NSObject>
- (void)productMethod;
@end

@protocol Factory1 <NSObject>

+ (id <ProductA>)createProductA:(NSString *)productName;

@end

@protocol Factory2 <NSObject>
+ (id <ProductB>)createProductB:(NSString *)productName;
@end

@interface FactoryA : NSObject <Factory1>
@end

@interface FactoryB : NSObject <Factory2>
@end

@interface ProductA1 : NSObject <ProductA>
@end

@interface ProductA2 : NSObject <ProductA>
@end

@interface ProductB1 : NSObject <ProductB>
@end

@interface ProductB2 : NSObject <ProductB>
@end

