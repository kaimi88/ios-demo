//
//  TemplateMethodDemo.h
//  模版方法模式(TemplateMethod)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

//1.定义: 定义一个操作中的算法的框架，而将一些步骤延迟到子类中。使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤。
//2. 使用场景： 1.多个子类有公有的方法，并且逻辑基本相同时。2.有重要、复杂的算法的时候，可以把核心算法设计为模板方法，周边的相关细节功能则由各个子类实现。
//3. 具体实现： 这里简单举了一个Android 和iOS项目的从code到发布的简易过程Demo
//4.优点： 1.封装不变部分，扩展可变部分。 2.提取公共代码，便于维护。 3.行为由父类控制，子类实现。
//5.缺点： 每一个不同的实现都需要一个子类来实现，导致类的个数增加，使得系统更加庞大。

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PublishProtocol <NSObject>

- (void)code;
- (void)test;
- (void)pack;
- (void)upload;

@end

@interface Publish : NSObject<PublishProtocol>

- (void)publish;

@end


@interface iOSPublish : Publish

@end

@interface AndroidPublish : Publish

@end

NS_ASSUME_NONNULL_END
