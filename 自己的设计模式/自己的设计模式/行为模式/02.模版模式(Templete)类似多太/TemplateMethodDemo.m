//
//  TemplateMethodDemo.m
//  模版方法模式(TemplateMethod)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import "TemplateMethodDemo.h"

@implementation Publish

- (void)publish {
    [self code];
    [self test];
    [self pack];
    [self upload];
}

- (void)code {
}
- (void)test {
}
- (void)pack {
}
- (void)upload {
}

@end

@implementation iOSPublish

- (void)code {
    NSLog(@"编写iOS代码");
}
- (void)test {
    NSLog(@"测试iOS代码");
}
- (void)pack {
    NSLog(@"xcode archive");
}
- (void)upload {
    NSLog(@"上传到AppStore");
}

@end

@implementation AndroidPublish

- (void)code {
    NSLog(@"编写Android代码");
}
- (void)test {
    NSLog(@"测试Android代码");
}
- (void)pack {
    NSLog(@"混淆代码，打包");
}
- (void)upload {
    NSLog(@"上传到各种应用商店");
}

@end
