//
//  ObserverDemo.h
//  观察者模式(Observer)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//



//一个对象的状态发生改变，所有的依赖对象都将得到通知的时候。
//1.观察者和被观察者是抽象耦合的，扩展比较方便。2.建立一套触发机制。

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol ObserverProtocol <NSObject>

- (void)update;

@end

@interface JobHunter : NSObject<ObserverProtocol>

- (instancetype)initWithName:(NSString *)name;

@end

@protocol SubjectProtocol <NSObject>

- (void)addObserver:(id <ObserverProtocol>)observer;
- (void)removeObserver:(id <ObserverProtocol>)observer;
- (void)notify;

@end

@interface JobProvider : NSObject<SubjectProtocol>


@end

NS_ASSUME_NONNULL_END
