//
//  ObserverDemo.m
//  观察者模式(Observer)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import "ObserverDemo.h"

@interface JobProvider()

@property (nonatomic, strong) NSMutableArray *observers;

@end

@implementation JobProvider

- (void)addObserver:(NSObject *)observer {
    [self.observers addObject:observer];
}

- (void)removeObserver:(NSObject *)observer {
    [self.observers removeObject:observer];
}

- (void)notify {
    for (id <ObserverProtocol> observer in self.observers) {
        [observer update];
    }
}

- (NSMutableArray *)observers {
    if (!_observers) {
        _observers = [[NSMutableArray alloc] init];
    }
    return _observers;
}

@end

@interface JobHunter()

@property (nonatomic, copy) NSString *name;

@end

@implementation JobHunter

- (instancetype)initWithName:(NSString *)name {
    if (self = [super init]) {
        _name = name;
    }
    return self;
}


- (void)update {
    NSLog(@"%@:有一个新的职位更新啦",_name);
}

@end
