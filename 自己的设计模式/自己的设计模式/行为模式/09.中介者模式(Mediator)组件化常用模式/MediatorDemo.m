//
//  MediatorDemo.m
//  中介者模式(Mediator)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import "MediatorDemo.h"

@implementation ChatRoom

- (void)showMessage:(NSString *)message {
    NSLog(@"%@\n",message);
}

- (void)showMessage:(NSString *)message userName:(NSString *)name {
    NSString *string = [NSString stringWithFormat:@"%@:%@", name, message];
    [self showMessage:string];
}

@end

@interface User()

@property (nonatomic, copy) NSString *name;     ///< 用户昵称
@property (nonatomic, strong) ChatRoom *room;   ///< 当前聊天室

@end


@implementation User

- (instancetype)initWithName:(NSString *)name room:(ChatRoom *)room {
    if (self = [super init]) {
        _name = name;
        _room = room;
    }
    return self;
}

- (void)sendMessage:(NSString *)message {
    [_room showMessage:message userName:_name];
}

@end

