//
//  PositivePerson.h
//  访问者模式(Visitor)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PersonProtocol.h"
//被访问元素
@interface PositivePerson:NSObject<PersonProtocol>

@end


