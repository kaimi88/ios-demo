//
//  HalfCupWater.h
//  访问者模式(Visitor)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ActionProtocol.h"

@interface HalfCupWater : NSObject<ActionProtocol>

@end
