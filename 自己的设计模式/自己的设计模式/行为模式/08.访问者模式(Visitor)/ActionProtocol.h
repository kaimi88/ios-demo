//
//  ActionProtocol.h
//  访问者模式(Visitor)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//


//访问者模式是一种对象行为型模式。（意思就是 即要有对象抽象类、又要有行为抽象类）

//优点：
//增加新的访问操作很方便。使用访问者模式，增加新的访问操作就意味着增加一个新的具体访问者类，实现简单，无须修改源代码，符合“开闭原则”。
//将有关元素对象的访问行为集中到一个访问者对象中，而不是分散在一个个的元素类中。类的职责更加清晰，有利于对象结构中元素对象的复用，相同的对象结构可以供多个不同的访问者访问。
//让用户能够在不修改现有元素类层次结构的情况下，定义作用于该层次结构的操作。


#import <Foundation/Foundation.h>

@protocol PersonProtocol;

@protocol ActionProtocol <NSObject>//抽象行为

- (void)positiveConclusion:(id <PersonProtocol>)positive;
- (void)negativeConclusion:(id <PersonProtocol>)negavite;

@end
