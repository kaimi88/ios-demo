//
//  PositivePerson.m
//  访问者模式(Visitor)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import "PositivePerson.h"

@implementation PositivePerson

- (void)accept:(id <ActionProtocol>)visitor {
    [visitor positiveConclusion:[[PositivePerson alloc] init]];
}

@end
