//
//  ObjectStructure.h
//  访问者模式(Visitor)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PersonProtocol.h"
#import "ActionProtocol.h"

@interface ObjectStructure : NSObject

- (void)add:(id <PersonProtocol>)person;
- (void)remove:(id <PersonProtocol>)person;
- (void)echo:(id <ActionProtocol>)action;

@end
