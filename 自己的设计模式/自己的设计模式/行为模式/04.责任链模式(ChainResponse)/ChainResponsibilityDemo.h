//
//  ChainResponsibilityDemo.h
//  责任链模式(ChainOfResponsibility)
//
//  Created by mark on 2020/2/11.
//  Copyright © 2020 mark. All rights reserved.
//


//2. 使用场景： 有多个对象可以处理同一个请求，具体哪个对象处理该请求由运行时确定。
//
//4.优点： 1.低耦合：将请求和处理分开，请求者可以不用知道是谁处理的。2.新增和修改新的处理类比较容易
//5.缺点： 每个请求都是从链头遍历到链尾，如果链比较长会产生一定的性能问题，调试起来也比较麻烦

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol Leave <NSObject>

- (void)handleLeaveApplication:(NSUInteger)dayCount;

@end

@interface Manager : NSObject<Leave>

@property (nonatomic, strong) id<Leave> superior;

@end

@interface CEO : Manager

@end

@interface CTO : Manager

@end

@interface PM : Manager

@end


NS_ASSUME_NONNULL_END
