//
//  AbaloneCommand.m
//  命令模式
//
//  Created by mark on 2020/2/9.
//  Copyright © 2020 mark. All rights reserved.
//

#import "AbaloneCommand.h"

@implementation AbaloneCommand


- (void)execute {
    [self.cook cookAbalone];
}


- (void)cancleCommand{
    [self.cook cancelAbalone];
}
@end
