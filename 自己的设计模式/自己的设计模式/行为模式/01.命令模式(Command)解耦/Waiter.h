//
//  Waiter.h
//  命令模式
//
//  Created by mark on 2020/2/9.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommandProtocol.h"
NS_ASSUME_NONNULL_BEGIN
/**
 服务员
 */
@interface Waiter : NSObject

/**
 点菜
 */
- (void)addOrder:(id <CommandProtocol>)command;

/**
 全点好了
 */
- (void)submmitOrder;

/**
 取消菜
 */
- (void)cancleOrder:(id <CommandProtocol>)command;

@end

NS_ASSUME_NONNULL_END
