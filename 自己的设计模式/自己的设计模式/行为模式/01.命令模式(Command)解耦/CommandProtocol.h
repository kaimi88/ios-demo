//
//  CommandProtocol.h
//  命令模式
//
//  Created by mark on 2020/2/9.
//  Copyright © 2020 mark. All rights reserved.
//

//1.定义: 命令模式将请求封装成对象，从而可用不同的请求对客户进行参数化，对请求排队或记录请求日志，以及支持可撤销和恢复的操作。
//2. 使用场景： 在某些场合，比如要对行为进行"记录、撤销/重做、事务"等处理的时候。
//3. 具体实现： YTKNetwork就是用的命令模式，推荐大家学习。这里我举了一个吃饭点菜的例子， 具体请点击这里查看
//4.优点： 1.类间解耦：调用者与接收者之间没有任何依赖关系。2.扩展性良好：新的命令可以很容易添加到系统中去。
//5.缺点： 使用命令模式可能会导致系统有过多的具体命令类。

//waiter(调用者) 和cook(接收者)之间没有任何依赖关系，很好的解耦、

#import <Foundation/Foundation.h>

@protocol CommandProtocol <NSObject>

@required
/**
 执行指令
 */
- (void)execute;

- (void)cancleCommand;


@end

