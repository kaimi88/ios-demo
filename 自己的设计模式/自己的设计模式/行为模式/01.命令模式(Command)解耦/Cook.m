//
//  Cook.m
//  命令模式
//
//  Created by mark on 2020/2/9.
//  Copyright © 2020 mark. All rights reserved.
//

#import "Cook.h"

@implementation Cook

- (void)cookLobster {
    NSLog(@"制作好了龙虾");
}

- (void)cancelLobster{
    NSLog(@"取消龙虾");
}

- (void)cookAbalone {
    NSLog(@"制作好了鲍鱼");
}

- (void)cancelAbalone {
    NSLog(@"取消鲍鱼");
}


@end
