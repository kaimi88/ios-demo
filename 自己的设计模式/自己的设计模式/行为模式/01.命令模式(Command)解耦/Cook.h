//
//  Cook.h
//  命令模式
//
//  Created by mark on 2020/2/9.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Cook : NSObject
/**
 制作龙虾
 */
- (void)cookLobster;

/**
 制作鲍鱼
 */
- (void)cookAbalone;


- (void)cancelLobster;

- (void)cancelAbalone;
@end

NS_ASSUME_NONNULL_END
