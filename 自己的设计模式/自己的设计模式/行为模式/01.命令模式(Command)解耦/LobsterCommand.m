//
//  LobsterCommand.m
//  命令模式
//
//  Created by mark on 2020/2/9.
//  Copyright © 2020 mark. All rights reserved.
//

#import "LobsterCommand.h"

@implementation LobsterCommand


- (void)execute {
    [self.cook cookLobster];
}

- (void)cancleCommand{
    [self.cook cancelLobster];
}


@end
