//
//  Command.m
//  命令模式
//
//  Created by mark on 2020/2/9.
//  Copyright © 2020 mark. All rights reserved.
//

#import "Command.h"

@implementation Command

- (void)execute {
}

- (void)cancleCommand {
}

- (instancetype)initWithReceiver:(Cook *)cook {
    if (self = [super init]) {
        _cook = cook;
    }
    return self;
}

@end
