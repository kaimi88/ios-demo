//
//  StateDemo.m
//  状态模式(State)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import "StateDemo.h"

@implementation LiveWait

- (void)liveState:(NSString *)state{
    NSLog(@"当前状态是:%@",state);
}

@end

@implementation LiveNow

- (void)liveState:(NSString *)state{
    NSLog(@"当前状态是:%@",state);
}

@end

@implementation LiveEnd

- (void)liveState:(NSString *)state{
    NSLog(@"当前状态是:%@",state);
}

@end

@implementation LiveStateManager

- (instancetype)initWithState:(id)state {
    if (self = [super init]) {
        self.state = state;
    }
    return self;
}

- (void)echo:(NSString *)currentState {
    [self.state liveState:currentState];
}

@end
