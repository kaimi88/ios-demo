//
//  StrategyDemo.m
//  策略模式(Strategy)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import "StrategyDemo.h"


@implementation BubbleSortStrategy

+ (void)sort:(NSArray *)array {
    NSLog(@"Array's count=%ld, 使用了冒泡排序", array.count);
}

@end

@implementation QuickSortStrategy

+ (void)sort:(NSArray *)array {
    NSLog(@"Array's count=%ld, 使用了快速排序", array.count);
}

@end

@implementation Sort

+ (void)sort:(NSArray *)array {
    if (array.count > 5) {
        [QuickSortStrategy sort:array];
    } else {
        [BubbleSortStrategy sort:array];
    }
}

@end
