//
//  StrategyDemo.h
//  策略模式(Strategy)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

//1.定义: 定义一系列的算法，把它们一个个封装起来，并且使它们可相互替换。
//2. 使用场景： 1.多个类只有在算法或行为上稍有不同的场景。2.算法需要自由切换的场景。3.需要屏蔽算法规则的场景。
//3. 具体实现： 具体请点击这里查看
//4.优点： 1.算法可以自由切换。 2.避免使用多重条件判断。 3.扩展性良好。
//5.缺点：1.策略类会增多。 2.所有策略类都需要对外暴露。
//6.注意事项: 如果一个系统的策略多于四个，就需要考虑使用混合模式，解决策略类膨胀的问题。

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol StrategyProtocol <NSObject>

+ (void)sort:(NSArray *)array;

@end

@interface BubbleSortStrategy : NSObject<StrategyProtocol>

@end

@interface QuickSortStrategy : NSObject<StrategyProtocol>

@end

@interface Sort : NSObject<StrategyProtocol>

@end

NS_ASSUME_NONNULL_END
