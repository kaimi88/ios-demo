//
//  MementoDemo.m
//  备忘录模式(Memento)
//
//  Created by mark on 2020/2/11.
//  Copyright © 2020 mark. All rights reserved.
//

#import "MementoDemo.h"

@implementation EditorMemento

- (instancetype)initWithArray:(NSArray *)array {
    if (self = [super init]) {
        _array = array;
    }
    return self;
}
@end

@interface Editor()

@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) EditorMemento *memento;

@end

@implementation Editor

- (instancetype)init {
    if (self = [super init]) {
        _array = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)insertContent:(NSString *)string {
    [_array addObject:string];
}

- (EditorMemento *)save {
    return [[EditorMemento alloc] initWithArray:[_array copy]];
}

- (void)echo {
    for (NSString *string in _array) {
        NSLog(@"%@", string);
    }
}

- (void)restore:(EditorMemento *)memento {
    _array = [[NSMutableArray alloc] initWithArray:memento.array];
}

- (EditorMemento *)memento {
    if (!_memento) {
        _memento = [[EditorMemento alloc] initWithArray:[_array copy]];
    }
    return _memento;
}

@end
