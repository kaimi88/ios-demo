//
//  ObjectStructure.m
//  访问者模式(Visitor)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import "ObjectStructure.h"

@interface ObjectStructure()

@property (nonatomic, strong) NSMutableArray <PersonProtocol> *array;

@end

@implementation ObjectStructure

- (instancetype)init {
    if (self = [super init]) {
        _array = [[NSMutableArray <PersonProtocol> alloc] init];
    }
    return self;
}

- (void)add:(id <PersonProtocol>)person {
    [_array addObject:person];
}

- (void)remove:(id <PersonProtocol>)person {
    [_array removeObject:person];
}

- (void)echo:(id <ActionProtocol>)action {//集合（对象结构）
    for (id <PersonProtocol>person in _array) {
        [person accept:action];
    }
}

@end
