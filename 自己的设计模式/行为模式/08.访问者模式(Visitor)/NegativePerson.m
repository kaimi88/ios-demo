//
//  NegativePerson.m
//  访问者模式(Visitor)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import "NegativePerson.h"

@implementation NegativePerson

- (void)accept:(id <ActionProtocol>)visitor {
    [visitor negativeConclusion:[[NegativePerson alloc] init]];
}

@end
