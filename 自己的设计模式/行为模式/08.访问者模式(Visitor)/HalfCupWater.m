//
//  HalfCupWater.m
//  访问者模式(Visitor)
//
//  Created by mark on 2020/2/10.
//  Copyright © 2020 mark. All rights reserved.
//

#import "HalfCupWater.h"
//访问者对对象
//将有关元素对象的访问行为集中到一个访问者对象中，而不是分散在一个个的元素类中。类的职责更加清晰，有利于对象结构中元素对象的复用，
@implementation HalfCupWater

- (void)positiveConclusion:(id <PersonProtocol>)positive {
    NSLog(@"乐观的人:还有半杯水可以喝");
}
- (void)negativeConclusion:(id <PersonProtocol>)negavite {
    NSLog(@"悲观的人:只剩半杯水了啊");
}

@end
