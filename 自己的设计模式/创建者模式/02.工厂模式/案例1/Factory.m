//
// Created by mark on 2020/2/3.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//

/*
简单工厂不同的是：在工厂方法模式中，因为创建对象的责任移交给了抽象工厂的子类，
因此客户端需要知道其所需产品所对应的工厂子类，而不是简单工厂中的参数。
 */

/*
1、抽象工厂（Abstract Factory）：抽象工厂负责声明具体工厂的创建产品的接口。
2、具体工厂（Concrete Factory）：具体工厂负责创建产品。
3、抽象产品（Abstract Product）：抽象产品是工厂所创建的所有产品对象的父类，负责声明所有产品实例所共有的公共接口。
4、具体产品（Concrete Product）：具体产品是工厂所创建的所有产品对象类，它以自己的方式来实现其共同父类声明的接口
。
 */

#import "Factory.h"

@implementation Factory_A
+ (id <ProductProtocol>)createProduct:(NSString *)productName {
    if ([productName isEqualToString:NSStringFromClass(Product_A.class)]) {
        return [[Product_A alloc] init];
    }
    return nil;
}

@end

@implementation Factory_B
+ (id <ProductProtocol>)createProduct:(NSString *)productName {
    if ([productName isEqualToString:NSStringFromClass(Product_B.class)]) {
        return [[Product_B alloc] init];
    }
    return nil;
}

@end

@implementation Product_A

- (void)productMethod {
    NSLog(@"生产A产品");
}

@end

@implementation Product_B
- (void)productMethod {
    NSLog(@"生产B产品");
}

@end
