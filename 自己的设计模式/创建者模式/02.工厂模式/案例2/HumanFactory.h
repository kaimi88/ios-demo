//
//  HumanFactory.h
//  MLDesignPatterns-OC
//
//  Created by mjpc on 2017/8/16.
//  Copyright © 2017年 mali. All rights reserved.
//


//1.定义: 定义一个用于创建对象的接口，让子类决定实例化哪一个类。
//2. 使用场景： 当存在多个类共同实现一个协议或者共同继承一个基类的时候，需要创建不同的对象，这个时候就可以考虑是否有必要使用工厂类进行管理。

#import <Foundation/Foundation.h>

@class Human;
@protocol HumanFactory <NSObject>

+ (Human *)createHuman;

@end
