//
// Created by mark on 2020/2/5.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//

#import "SimpleBuilder.h"

@implementation Phone

- (void)getDescription{
    NSLog(@"%@,%@,%@,%@",self.cpu,self.capacity,self.camera,self.display);
}
@end

@interface IPhoneXRBuilder()
{
     Phone *_phone;
}
@end

@implementation IPhoneXRBuilder

- (void)createPhone {
    _phone = [[Phone alloc] init];
}

- (void)buildCPU {
    [_phone setCpu:@"A12"];
}

- (void)buildCapacity {
    [_phone setCapacity:@"256"];
}

- (void)buildDisplay {

    [_phone setDisplay:@"6.1"];
}

- (void)buildCamera {
    [_phone setCamera:@"12MP"];
}

- (Phone *)obtainPhone {
    return _phone;
}

@end

@interface MIPhoneXRBuilder ()
{
    Phone *_phone;
}

@end
@implementation MIPhoneXRBuilder
- (void)createPhone {
    _phone = [[Phone alloc] init];
}

- (void)buildCPU {
    [_phone setCpu:@"Snapdragon 845"];
}

- (void)buildCapacity {
    [_phone setCapacity:@"128"];
}

- (void)buildDisplay {
    [_phone setDisplay:@"6.21"];
}

- (void)buildCamera {
    [_phone setCamera:@"12MP"];
}

- (Phone *)obtainPhone {
    return _phone;
}
@end

@implementation Director
+ (Phone *)constructPhoneWithBuilder:(id<Builder>)builder {
    [builder createPhone];
    [builder buildCPU];
    [builder buildCapacity];
    [builder buildDisplay];
    [builder buildCamera];
    Phone *phone = [builder obtainPhone];
    return phone;
}
@end
