//
// Created by mark on 2020/2/4.
// Copyright (c) 2020 ___FULLUSERNAME___. All rights reserved.
//

#import "AbastractFactory.h"

@class ProductA1, ProductA2, ProductB1, ProductB2, FactoryA, FactoryB;

@implementation FactoryA
+ (id <ProductA>)createProductA:(NSString *)productName {
    if([productName isEqualToString:NSStringFromClass(ProductA1.class)]){
        return [[ProductA1 alloc] init];
    }else if ([productName isEqualToString:NSStringFromClass(ProductA2.class)]){
        return [[ProductA1 alloc] init];
    }
    return nil;
}
@end

@implementation FactoryB
+ (id <ProductB>)createProductB:(NSString *)productName {
    if([productName isEqualToString:NSStringFromClass(ProductB1.class)]){
        return [[ProductB1 alloc] init];
    }else if ([productName isEqualToString:NSStringFromClass(ProductB2.class)]){
        return [[ProductB2 alloc] init];
    }
    return nil;
}

@end

@implementation ProductA1
- (void)productMethod {
    NSLog(@"生产的华为手机");
}

@end

@implementation ProductA2
- (void)productMethod {
    NSLog(@"生产的华为电脑");
}
@end

@implementation ProductB1
- (void)productMethod {
    NSLog(@"生产的小米手机");
}

@end

@implementation ProductB2
- (void)productMethod {
     NSLog(@"生产的h小米电脑");
}
@end
