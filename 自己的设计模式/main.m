//
//  main.m
//  自己的设计模式
//
//  Created by mark on 2020/2/11.
//  Copyright © 2020 mark. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Human.h"
#import "WhiteHumanFactory.h"
#import "YellowHumanFactory.h"
#import "BlackHumanFactory.h"
#import "Door.h"
#import "DoorInstaller.h"
#import "WoodDoorFactory.h"
#import "IronDoorFactory.h"
#import "SimpleFactory.h"
#import "SimpleBuilder.h"
#import "FiveYuanNoSpicyBuild.h"
#import "TenYuanAbnormalSpicyBuild.h"
#import "Seller.h"
#import "Company.h"
#import "Employee.h"
#import "EmployeeDeepCopy.h"
#import "EmployeeShallowCopy.h"
#import "PrototypeDemo.h"
#import "CommandProtocol.h"
#import "Waiter.h"
#import "AbaloneCommand.h"
#import "LobsterCommand.h"
#import "StrategyDemo.h"
#import "ChainResponsibilityDemo.h"
#import "StateDemo.h"
#import "ObserverDemo.h"
#import "ObjectStructure.h"
#import "HalfCupWater.h"
#import "PositivePerson.h"
#import "NegativePerson.h"
#import "MediatorDemo.h"
#import "ProxyDemo.h"
#import "WrapperDemo.h"
#import "WrapperDemo2.h"
#import "BridgeDesign.h"
#import "FacadeDemo.h"
#import "SimpleDecorator.h"

void simplefactory(){
    //一个工厂就可以生产不同产品
    [[SimpleFactory createProduct:@"Product_AA"] productMethod];
    [[SimpleFactory createProduct:@"Product_BB"] productMethod];
}
void factory(){
    //不同工厂 生产不同产品
    Human *whiteMan = [WhiteHumanFactory createHuman];
    [whiteMan talk];
    Human *yellowMan = [YellowHumanFactory createHuman];
    [yellowMan talk];
    Human *blackMan = [BlackHumanFactory createHuman];
    [blackMan talk];
}

void abstractFactory() {
    //不同工厂 生产不同产品的同时也可以对外暴露接口生产同一系列不同类型的产品
    Door *woodDoor = [WoodDoorFactory createDoor];
    DoorInstaller *woodDoorInstaller = [WoodDoorFactory createDoorInstall];
    [woodDoor getDescription];
    [woodDoorInstaller getDescription];
    
    Door *ironDoor = [IronDoorFactory createDoor];
    DoorInstaller *ironDoorInstaller = [IronDoorFactory createDoorInstall];
    [ironDoor getDescription];
    [ironDoorInstaller getDescription];
}

void bulider(){
    IPhoneXRBuilder *iphonexrBuilder = [[IPhoneXRBuilder alloc] init];
    
    MIPhoneXRBuilder *miphoneBuilder = [[MIPhoneXRBuilder alloc] init];
    
    Phone *phone1 = [Director constructPhoneWithBuilder:iphonexrBuilder];
    [phone1 getDescription];
    Phone *phone2 = [Director constructPhoneWithBuilder:miphoneBuilder];
    [phone2 getDescription];
}

void builder2() {
    FiveYuanNoSpicyBuild *fiveBuilder = [[FiveYuanNoSpicyBuild alloc] init];
    TenYuanAbnormalSpicyBuild *tenBuilder = [[TenYuanAbnormalSpicyBuild alloc] init];
    Seller *aSeller = [[Seller alloc] init];
    aSeller.handWheatCakeBuilder = fiveBuilder;
    [aSeller cookFood];
    aSeller.handWheatCakeBuilder = tenBuilder;
    [aSeller cookFood];
}

void prototype1(){
    StudentModel *stu1 = [StudentModel new];
    stu1.name = @"张三";
       
    StudentModel *stu2 = stu1.copy;
    stu2.name = @"李4";
       
       // classModel
    ClassModel *class1 = [[ClassModel alloc] init];
    class1.className = @"ban ji 1";
    class1.students = @[stu1, stu2];
       
    ClassModel *class2 = class1.copy;
    NSLog(@"%@ ----%@", class1, class2);
       
    NSLog(@"%@", class1.students);
    NSLog(@"%@", class2.students);
}

void prototype2() {
    Company *sina = [[Company alloc] initWithName:@"新浪" establishmentTime:@"2014-08-08" level:@"上市"];
    Company *alibaba = [[Company alloc] initWithName:@"阿里巴巴" establishmentTime:@"2008-08-08" level:@"上市"];
    EmployeeShallowCopy *zhangSan = [[EmployeeShallowCopy alloc] initWithName:@"张三" configWithAge:20 department:@"IT" company:sina];
    EmployeeDeepCopy *zhangsanDeep = [[EmployeeDeepCopy alloc] initWithName:@"张三deep" configWithAge:20 department:@"IT" company:alibaba];
    EmployeeShallowCopy *liSi = [zhangSan copy];
    sina.name = @"浪里个浪";
    NSLog(@"%@", liSi.company.name);
    EmployeeDeepCopy *wangWu = [zhangsanDeep copy];
    alibaba.name = @"新阿里巴巴";
    NSLog(@"%@", wangWu.company.name);
}

void commmand(){
    Cook *cook = [[Cook alloc] init];
    AbaloneCommand *loneCommand = [[AbaloneCommand alloc] initWithReceiver:cook];
    
    LobsterCommand *lobsterCommand = [[LobsterCommand alloc] initWithReceiver:cook];
    Waiter *waiter = [[Waiter alloc] init];
    [waiter addOrder:loneCommand];
    [waiter addOrder:lobsterCommand];
    [waiter submmitOrder];//提交后的事物不能取消
    [waiter cancleOrder:lobsterCommand];
}

void strategy(){
    NSArray *array = @[@"1",@"2",@"3",@"4",@"5",@"6"];
    [Sort sort:array];
}

void chainResponseDemo(){
    CEO *ceo = [[CEO alloc] init];
    CTO *cto = [[CTO alloc] init];
    cto.superior = ceo;
    PM *pm = [[PM alloc] init];
    pm.superior = cto;
    [pm handleLeaveApplication:20];//根据
}

void stateDemo(){
    LiveStateManager *editor = [[LiveStateManager alloc] initWithState:[[LiveWait alloc] init]];
    [editor echo:@"当前状态待直播"];
    editor.state = [[LiveNow alloc] init];
    [editor echo:@"直播中"];
    editor.state = [[LiveEnd alloc] init];
    [editor echo:@"结束直播"];
}

void observerDemo(){
    JobHunter *zhangSan = [[JobHunter alloc] initWithName:@"张三"];
    JobHunter *liSi = [[JobHunter alloc] initWithName:@"李四"];
    JobProvider *jobProvider = [[JobProvider alloc] init];
    [jobProvider addObserver:zhangSan];
    [jobProvider addObserver:liSi];
    [jobProvider notify];
}


void visitor(){
    ObjectStructure *structure = [[ObjectStructure alloc] init];
    [structure add:[[PositivePerson alloc] init]];
    [structure add:[[NegativePerson alloc] init]];
    HalfCupWater *action = [[HalfCupWater alloc] init];
    [structure echo:action];
}

void mediatorDemo(){
    ChatRoom *room = [[ChatRoom alloc] init];//chatRoom就相当于中介对象，封装了对外交互的逻辑
    User *wuJun = [[User alloc] initWithName:@"吴军" room:room];
    User *me = [[User alloc] initWithName:@"SuperMario" room:room];
    [wuJun sendMessage:@"来自硅谷的第一封信"];
    [me sendMessage:@"谢谢，不做伪工作者"];
}

void proxydemo(){
    TicketOutlets *ticketOuts = [[TicketOutlets alloc] init];
    [ticketOuts sell];
    
}

void adapterDemo(){
    AdapterUSD *adapterUsd = [[AdapterUSD alloc] init];
    AdapterManager *manager = [[AdapterManager alloc] initWithAdapter:adapterUsd];
    [manager getCNY];
}

void bridgeDesign(){
    BlueTheme *blueTheme = [[BlueTheme alloc] init];
    RedTheme *redTheme = [[RedTheme alloc] init];
    
    [ChatModule moduleDescriptionWithTheme:blueTheme];
    
    [MineModule moduleDescriptionWithTheme:blueTheme];
    
    [MineModule moduleDescriptionWithTheme:redTheme];
}

void facadeDemo(){
    HouseManager *manager = [[HouseManager alloc] init];
    [manager sleep];
    [manager getUp];
}

void decorader(){
    DarkRoast *darkRost = [[DarkRoast alloc] init];
    Milk *milk = [[Milk alloc] initWithBeverage:darkRost];
    [milk getName];
    [milk cost];
}


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        
//        simplefactory();
//        factory();
//        abstractFactory();
//        bulider();
//        builder2();
//        prototype1();
//        commmand();
//        strategy();
//        chainResponseDemo();
//        stateDemo();
//        observerDemo();
//        visitor();
//        mediatorDemo();
//        proxydemo();
//        adapterDemo();
//        bridgeDesign();
//        facadeDemo();
        decorader();
    }
    return NSApplicationMain(argc, argv);
}
